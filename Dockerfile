# Use the base image you need
FROM ubuntu:latest

WORKDIR /var/www/html/dockerproject

COPY . /var/www/html/dockerproject

CMD [ "echo", "Docker container is running" ]
